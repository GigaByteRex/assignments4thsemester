package compression;

import java.util.ArrayList;

public class Dictionary {

	ArrayList<String> words = new ArrayList<String>();
	ArrayList<String> binary = new ArrayList<String>();
	int bitSize = 5;
	
	public Dictionary(){
		initializeWords();
		initializeBinary();
	}
	
	public void initializeWords(){
		char[] alphabet = "#abcdefghijklmnopqrstuvwxyzæøå ".toCharArray();
		for(int i = 0; i < alphabet.length; i++){
			String letter = String.valueOf(alphabet[i]);
			words.add(letter);
		}
	}
	
	public void initializeBinary(){
		for(int i = 0; i < words.size(); i++){
			binary.add(convertIntToBinaryStringWithLength(i, bitSize));
		}
	}
	
	public String convertIntToBinaryStringWithLength(int num, int len){
		StringBuffer binaryString = new StringBuffer();
		binaryString.append(Integer.toString(num, 2));
		
		int zeroNum = len - binaryString.length();
		
		while(zeroNum-- > 0)
			binaryString.insert(0, "0");
		
		return binaryString.toString();
	}
	
	public ArrayList<String> getWords(){
		return words;
	}
	
	public ArrayList<String> getBinary(){
		return binary;
	}
	
	public int getBitSize(){
		return bitSize;
	}
	
	public void addEntry(String word){
		words.add(word);
		
		binary.add(convertIntToBinaryStringWithLength(binary.size(), bitSize));
		
		checkForBitSwitch();
	}
	
	public void removeLastEntry(){
		words.remove(words.size() - 1);
		binary.remove(binary.size() - 1);
	}
	
	public boolean checkForBitSwitch(){
		String lastBinaryEntry = binary.get(binary.size() - 1);
		
		for(int i = 0; i < lastBinaryEntry.length(); i++){
			if(lastBinaryEntry.charAt(i) == '0')
				break;
			
			if(i == lastBinaryEntry.length() - 1){
				bitSize++;
				matchBinaryRepresentationToBitSize();
				return true;
			}
		}
		return false;
	}
	
	public boolean checkNextBitSwitch(){
		int now = getBitSize();
		
		addEntry("check");
		
		int next = getBitSize();
		
		removeLastEntry();
		
		if(now != next){
			bitSize--;
			return true;
		}
		
		return false;
	}
	
	public void matchBinaryRepresentationToBitSize(){
		for(int i = 0; i < binary.size(); i++){
			binary.set(i, convertIntToBinaryStringWithLength(i, bitSize));
		}
	}
	
	@Override
	public String toString(){
		StringBuilder stringDictionary = new StringBuilder();
		for(int i = 0; i < words.size(); i++){
			stringDictionary.append("Word: " + words.get(i) + " + binary index: " + binary.get(i) + "\n");
		}
		
		return stringDictionary.toString();
	}
}
