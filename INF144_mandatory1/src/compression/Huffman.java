package compression;

import java.util.ArrayList;
import java.util.Map;
import java.util.PriorityQueue;

public class Huffman {
    // input is an array of frequencies, indexed by character code
    public static HuffmanTree buildTree(int[] charFreqs, ArrayList<String> uniques) {
        PriorityQueue<HuffmanTree> trees = new PriorityQueue<>();
        // initially, we have a forest of leaves
        // one for each non-empty character
        for (int i = 0; i < charFreqs.length; i++)
            if (charFreqs[i] > 0)
                trees.offer(new HuffmanLeaf(charFreqs[i], uniques.get(i)));

        assert trees.size() > 0;
        // loop until there is only one tree left
        while (trees.size() > 1) {
            // two trees with least frequency
            HuffmanTree a = trees.poll();
            HuffmanTree b = trees.poll();

            // put into new node and re-insert into queue
            trees.offer(new HuffmanNode(a, b));
        }
        return trees.poll();
    }

    public static Map buildMap(HuffmanTree tree, StringBuffer prefix, Map<String,String> symbolToHuffman) {

        assert tree != null;

        if (tree instanceof HuffmanLeaf) {
            HuffmanLeaf leaf = (HuffmanLeaf)tree;

            symbolToHuffman.put(leaf.value, prefix.toString());

        } else if (tree instanceof HuffmanNode) {
            HuffmanNode node = (HuffmanNode)tree;

            // traverse left
            prefix.append('0');
            buildMap(node.left, prefix,symbolToHuffman);
            prefix.deleteCharAt(prefix.length()-1);

            // traverse right
            prefix.append('1');
            buildMap(node.right, prefix,symbolToHuffman);
            prefix.deleteCharAt(prefix.length()-1);
        }
    return symbolToHuffman;
    }


}