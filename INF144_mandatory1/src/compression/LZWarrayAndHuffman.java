package compression;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import compression.Huffman;
import compression.HuffmanTree;
import compression.HuffmanLeaf;
import compression.HuffmanLeaf;

public class LZWarrayAndHuffman {

	public static void main(String[] args) {
		
		String story = "src/compression/folktale.txt";
		String compressed = "src/compression/compressed.txt";
		String decompressed = "src/compression/decompstory.txt";
		String huffman = "src/compression/huffman.txt";
		
		compress(story, compressed);
		decompress(compressed, decompressed);
		huffmanCode(compressed, huffman);
		
		int storySize = countContentLength("src/compression/folktale.txt");
		int compressedSize = countContentLength("src/compression/compressed.txt");
		int huffmanSize = countContentLength("src/compression/huffman.txt");
		
		double compressionRate = (double) storySize / (double) compressedSize;
		double huffmanRate = (double) storySize / (double) huffmanSize;
		
		System.out.println("Compression rate of LZW is: " + (1.00 - compressionRate) * 100.0 + "%");
		System.out.println("Compression rate of LZW + huffman is: " + (1.00 - huffmanRate) * 100.0 + "%");
	}
	
	public static void compress(String sourceFilePath, String destFilePath){
		BufferedReader reader;
		BufferedWriter writer;
		
		
		try{
			reader = new BufferedReader(new FileReader(sourceFilePath));
			writer = new BufferedWriter(new FileWriter(destFilePath, false));
			
			ArrayList<String> entries = initializeDictionary();
			
			String current, next, story;
			
			while((story = reader.readLine()) != null){
				story = story.toLowerCase();
				int count = 0;
				
				while(count < story.length() - 1){
					current = story.charAt(count) + "";
					next = story.charAt(++count) + "";
					
					while(entries.contains(current + next)){
						current = current + next;
						count++;
						
						if(count < story.length())
							next = story.charAt(count) + "";
						else
							next = null;
					}
					
					entries.add(current + next);
					
					writer.write(entries.indexOf(current) + " ");
				}
				writer.write("\n");
			}
			reader.close();
			writer.close();
			return;
		}
		catch(FileNotFoundException e){
			System.out.println("Couldn't find source file");
			return;
		}
		catch(IOException e){
			System.out.println("Error while writing output");
			return;
		}
	}
	
	public static ArrayList<String> initializeDictionary(){
		ArrayList<String> words = new ArrayList<String>();
		char[] alphabet = "#abcdefghijklmnopqrstuvwxyzæøå ".toCharArray();
		for(int i = 0; i < alphabet.length; i++){
			String letter = String.valueOf(alphabet[i]);
			words.add(letter);
		}
		
		return words;
	}
	
	public static void decompress(String sourceFilePath, String destFilePath){
		BufferedReader reader;
		BufferedWriter writer;
		Scanner in;
		try{
			reader = new BufferedReader(new FileReader(sourceFilePath));
			in = new Scanner(reader);
			writer = new BufferedWriter(new FileWriter(destFilePath, false));
			
			ArrayList<String> entires = initializeDictionary();
			
			String compressed, current, next;
			String[] story;
			Integer[] ints;
			
			while(in.hasNextLine()){
				compressed = in.nextLine();
				story = compressed.split(" ");
				ints = new Integer[story.length];
				
				for(int i = 0; i < ints.length; i++)
					ints[i] = Integer.parseInt(story[i]);
				
				for(int i = 0; i < ints.length; i++){
					current = entires.get(ints[i]);
					if((i + 1) < ints.length && ints[i + 1] < entires.size()){
						next = entires.get(ints[i + 1]);
						next = next.charAt(0) + "";
					}
					else if((i + 1) >= ints.length)
						next = null;
					else
						next = current.charAt(0) + "";
					
					entires.add(current + next);
					writer.write(current);
				}
				writer.write("\n");
			}
			System.out.println(entires.toString());
			in.close();
			reader.close();
			writer.close();
			return;
		}
		catch(FileNotFoundException e){
			System.out.println("Couldn't find source file");
			return;
		}
		catch(IOException e){
			System.out.println("Error while writing output");
			return;
		}
	}
	
	public static void huffmanCode(String sourceFilePath, String destFilePath){
		BufferedReader reader;
		BufferedWriter writer;
		
		
		try{
			reader = new BufferedReader(new FileReader(sourceFilePath));
			writer = new BufferedWriter(new FileWriter(destFilePath, false));
			
			String compressed = reader.readLine();
			
			String[] compressedArray;
			
			if(compressed.length() > 0){
				compressedArray = compressed.split(" ");
			}
			else{
				writer.write("");
				return;
			}
			
			ArrayList<String> compressedArrayList = new ArrayList<String>();
			
			for(String entry : compressedArray)
				compressedArrayList.add(entry);
			
			ArrayList<String> huffman = huffmanCoding(compressedArrayList);
			
			for(String entry : huffman)
				writer.write(entry);
			
			reader.close();
			writer.close();
			return;
		}
		catch(FileNotFoundException e){
			System.out.println("Couldn't find source file");
			return;
		}
		catch(IOException e){
			System.out.println("Error while writing output");
			return;
		}
	}
	
    public static ArrayList<String> huffmanCoding(ArrayList<String> inList){

        ArrayList<String> uniques = new ArrayList<>();
        ArrayList<String> result = new ArrayList<>();

        inList.forEach(item->{
            if(!uniques.contains(item)){ uniques.add(item);}
        });
        int[] charFreqs = new int[uniques.size()];

        // record the frequencies of each bitString
        inList.forEach(item-> charFreqs[uniques.indexOf(item)]++);


        HuffmanTree tree = Huffman.buildTree(charFreqs, uniques);
        Map<String,String> resMap = new HashMap<>();

        Map <String,String> newMap  = Huffman.buildMap(tree, new StringBuffer(),resMap);

        inList.forEach(item-> result.add(newMap.get(item)));

        return result;
    }
    
    public static int countContentLength(String filePath){
		BufferedReader reader;
		
		try{
			reader = new BufferedReader(new FileReader(filePath));
			
			String content = reader.readLine();
			
			int result = content.length();
			
			reader.close();
			return result;
		}
		catch(FileNotFoundException e){
			System.out.println("Couldn't find source file");
			return -1;
		}
		catch(IOException e){
			System.out.println("IOError");
			return -1;
		}
    }
}
