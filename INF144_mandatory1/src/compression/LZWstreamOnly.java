package compression;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import compression.Dictionary;

public class LZWstreamOnly {
	
	public static void main(String[] args) throws Exception{
		Dictionary dictionary = new Dictionary();
		
		String cleartext = getStringFromFile("src/compression/folktale.txt").toLowerCase();
		int originalTextSize = cleartext.getBytes("UTF-16BE").length * 8;
		
		String encodedText = LZWcompress(cleartext, dictionary);
		int encodedTextLength = encodedText.length();
		writeToFile(encodedText, "src/compression/compressed.txt");
		
		writeToFile(dictionary.toString(), "src/compression/dict.txt");
		
		double textLen = (double)originalTextSize;
		double encodeLen = (double)encodedTextLength;
		
		System.out.println("Original size: " + textLen + " bits.");
		System.out.println("New size: " + encodeLen + " bits.");
		
		double compressionRate = encodeLen / textLen;
		
		System.out.println("Compression rate: " + (1 - compressionRate) * 100 + "%");
		
		Dictionary newDict = new Dictionary();
		
		String decodedText = LZWdecompress(encodedText, newDict);
		
		writeToFile(decodedText, "src/compression/decompstory.txt");
	}
	
	public static String LZWcompress(String text, Dictionary dict){
		StringBuilder compressed = new StringBuilder();
		
		String current, next;
		int count = 0;
		
		while(count < text.length()){
			current = text.charAt(count) + "";
			next = text.charAt(++count) + "";
			
			while(dict.getWords().contains(current + next)){
				current = current + next;
				count++;
				
				if(count < text.length())
					next = text.charAt(count) + "";
				else
					next = null;
			}
			String binary = dict.getBinary().get(dict.getWords().indexOf(current));
			compressed.append(binary);
			dict.addEntry(current + next);
		}
		return compressed.toString();
	}
	
	public static String LZWdecompress(String text, Dictionary dict){
		StringBuilder decompressed = new StringBuilder();
		String currentBinary = "";
		int current = 0;
		ArrayList<String> conjecture = new ArrayList<String>();
		
		while(current < text.length()){
			currentBinary = "";
			
			for(int j = 0; j < dict.getBitSize(); j++)
				currentBinary += text.charAt(current++) + "";
			
			int index = dict.getBinary().indexOf(currentBinary);
			
			conjecture.add(dict.getWords().get(index));
			
			if(conjecture.size() > 1){
				String newEntry = conjecture.get(0) + conjecture.get(1);
				conjecture.remove(0);
				dict.addEntry(newEntry);
			}
			
			dict.addEntry("test");
			dict.removeLastEntry();
			
			decompressed.append(dict.getWords().get(index));
		}
		return decompressed.toString();
	}
	
	public static String getStringFromFile(String filepath) throws Exception{
		
		try(FileInputStream inputStream = new FileInputStream(filepath)){
			String text = IOUtils.toString(inputStream, "UTF-8");
			return text;
		}

	}
	
	public static void writeToFile(String text, String filepath) throws Exception{
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream(filepath), "utf-8"))) {
			writer.write(text);
		}
	}
}
