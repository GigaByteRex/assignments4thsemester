package compression;

import compression.LZWarrayAndHuffman;
import randomText.*;

public class hundredTextAndCompress{
	
	public static void main(String[] args) throws Exception{
		String text = GenerateText.getStringFromFile("src/compression/folktale.txt").toLowerCase();
		
		MarkovChain mchain = new MarkovChain(text);
		
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 100; j++){
				if(i == 0){
					String zero = GenerateText.zerothOrder(mchain, text);
					System.out.println("Zeroth order number " + j + ":");
					calculateAndPrint(zero);
					System.out.println();
				}
				else if(i == 1){
					String first = GenerateText.firstOrder(mchain, text);
					System.out.println("First order number " + j + ":");
					calculateAndPrint(first);
					System.out.println();
				}
				else if(i == 2){
					String second = GenerateText.secondOrder(mchain, text);
					System.out.println("Second order number " + j + ":");
					calculateAndPrint(second);
					System.out.println();
				}
				else{
					String third = GenerateText.thirdOrder(mchain, text);
					System.out.println("third order number " + j + ":");
					calculateAndPrint(third);
					System.out.println();
				}
			}
		}
	}
	
	public static void calculateAndPrint(String text) throws Exception{
		LZWstreamOnly.writeToFile(text, "src/compression/generate.txt");
		LZWarrayAndHuffman.compress("src/compression/generate.txt", "src/compression/100encode.txt");
		LZWarrayAndHuffman.huffmanCode("src/compression/100encode.txt", "src/compression/100huffman.txt");
		
		int storySize = LZWarrayAndHuffman.countContentLength("src/compression/generate.txt");
		int compressedSize = LZWarrayAndHuffman.countContentLength("src/compression/100encode.txt");
		int huffmanSize = LZWarrayAndHuffman.countContentLength("src/compression/100huffman.txt");
		
		double compressionRate = (double) storySize / (double) compressedSize;
		double huffmanRate = (double) storySize / (double) huffmanSize;
		
		System.out.println("Compression rate of LZW is: " + (1.00 - compressionRate) * 100.0 + "%");
		System.out.println("Compression rate of LZW + huffman is: " + (1.00 - huffmanRate) * 100.0 + "%");
	}
}
