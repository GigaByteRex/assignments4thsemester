package randomText;

import java.io.FileInputStream;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import Jama.Matrix;

public class GenerateTextv2 {

	public static void main(String[] args) throws Exception{
		
		String text = getStringFromFile("src/compression/folktale.txt").toLowerCase();

		MarkovChainv2 zeroChain = new MarkovChainv2(text, 0);
		MarkovChainv2 firstChain = new MarkovChainv2(text, 1);
		MarkovChainv2 secondChain = new MarkovChainv2(text, 2);
		MarkovChainv2 thirdChain = new MarkovChainv2(text, 3);
		
		System.out.println("Zeroth order: \n" + zerothOrder(zeroChain, text) + "\n");
		System.out.println("First order: \n" + firstOrder(firstChain, text) + "\n");
		System.out.println("Second order: \n " + secondOrder(secondChain, text) + "\n");
		System.out.println("Third order: \n " + thirdOrder(thirdChain, text) + "\n");
		
		printEntropy(zeroChain);
		printEntropy(firstChain);
		printEntropy(secondChain);
		//printEntropy(thirdChain);

	}

	public static String getStringFromFile(String filepath) throws Exception{

		try(FileInputStream inputStream = new FileInputStream(filepath)){
			String text = IOUtils.toString(inputStream, "UTF-8");
			return text;
		}

	}

	public static String zerothOrder(MarkovChainv2 mchain, String text){
		char[] alphabet = mchain.getAlphabet();
		double[] prob = new double[alphabet.length];

		ArrayList<String> searchSpace = new ArrayList<>();

		for(char letter : alphabet)
			searchSpace.add(letter + "");

		for(int i = 0; i < text.length(); i++){
			prob[searchSpace.indexOf(text.charAt(i) + "")]++;
		}

		for(int i = 0; i < prob.length; i++){
			double current = prob[i];
			prob[i] = current / (double) text.length();
		}

		StringBuffer result = new StringBuffer();

		for(int i = 0; i < text.length(); i++){
			double num = Math.random();
			double sum = 0.0;

			for(int j = 0; j < prob.length; j++){
				sum += prob[j];

				if(sum >= num){
					result.append(alphabet[j]);
					break;
				}
			}
		}
		return result.toString();
	}

	public static String firstOrder(MarkovChainv2 mchain, String text){
		StringBuffer result = new StringBuffer();

		String currentLetter = mchain.getSearchSpace().get((int)Math.floor(Math.random() * mchain.getSearchSpace().size()));
		int check = mchain.getSearchSpace().indexOf(currentLetter);
		double checksum = 0.0;

		while(checksum == 0.0){
			for(int j = 0; j < mchain.getSearchSpace().size(); j++){
				checksum += mchain.getTransitionMatrix().get(check, j);
			}
			if(checksum == 0.0){
				currentLetter = mchain.getSearchSpace().get((int)Math.floor(Math.random() * mchain.getSearchSpace().size()));
				check = mchain.getSearchSpace().indexOf(currentLetter);
			}
		}

		int current = mchain.getSearchSpace().indexOf(currentLetter);

		for(int i = 0; i < text.length(); i++){
			if(i != 0)
				current = mchain.getSearchSpace().indexOf(currentLetter);

			double num = Math.random();
			double sum = 0.0;

			for(int j = 0; j < mchain.getSearchSpace().size(); j++){
				sum += mchain.getTransitionMatrix().get(current, j);

				if(sum >= num){
					currentLetter = mchain.getSearchSpace().get(j);
					result.append(mchain.getSearchSpace().get(j));
					break;
				}
			}
		}
		return result.toString();
	}

	public static String secondOrder(MarkovChainv2 mchain, String text){
		
		StringBuffer result = new StringBuffer();

		Matrix transitionMatrix = mchain.getTransitionMatrix();

		String currentLetter = mchain.getSearchSpace().get((int)Math.floor(Math.random() * mchain.getSearchSpace().size()));
		int check = mchain.getSearchSpace().indexOf(currentLetter);
		double checksum = 0.0;

		while(checksum == 0.0){
			for(int j = 0; j < mchain.getTransitionMatrix().getColumnDimension(); j++){
				checksum += mchain.getTransitionMatrix().get(check, j);
			}
			if(checksum == 0.0){
				currentLetter = mchain.getSearchSpace().get((int)Math.floor(Math.random() * mchain.getSearchSpace().size()));
				check = mchain.getSearchSpace().indexOf(currentLetter);
			}
		}

		int current = mchain.getSearchSpace().indexOf(currentLetter);

		for(int i = 0; i < text.length(); i++){
			if(i != 0)
				current = mchain.getSearchSpace().indexOf(currentLetter);

			double num = Math.random();
			double sum = 0.0;

			for(int j = 0; j < mchain.getTransitionMatrix().getColumnDimension(); j++){
				sum += transitionMatrix.get(current, j);

				if(sum >= num){
					result.append(mchain.getAlphabet()[j]);
					currentLetter = currentLetter.substring(1, 2) + mchain.getAlphabet()[j] + "";
					break;
				}
			}
		}
		return result.toString();
	}

	public static String thirdOrder(MarkovChainv2 mchain, String text){
		StringBuffer result = new StringBuffer();

		Matrix transitionMatrix = mchain.getTransitionMatrix();

		String currentLetter = mchain.getSearchSpace().get((int)Math.floor(Math.random() * mchain.getSearchSpace().size()));
		int check = mchain.getSearchSpace().indexOf(currentLetter);
		double checksum = 0.0;

		while(checksum == 0.0){
			for(int j = 0; j < mchain.getTransitionMatrix().getColumnDimension(); j++){
				checksum += mchain.getTransitionMatrix().get(check, j);
			}
			if(checksum == 0.0){
				currentLetter = mchain.getSearchSpace().get((int)Math.floor(Math.random() * mchain.getSearchSpace().size()));
				check = mchain.getSearchSpace().indexOf(currentLetter);
			}
		}

		int current = mchain.getSearchSpace().indexOf(currentLetter);

		for(int i = 0; i < text.length(); i++){
			if(i != 0)
				current = mchain.getSearchSpace().indexOf(currentLetter);

			double num = Math.random();
			double sum = 0.0;

			for(int j = 0; j < mchain.getTransitionMatrix().getColumnDimension(); j++){
				sum += transitionMatrix.get(current, j);

				if(sum >= num){
					result.append(mchain.getAlphabet()[j]);
					currentLetter = currentLetter.substring(1, 3) + mchain.getAlphabet()[j] + "";
					break;
				}
			}
		}
		return result.toString();
	}
	
	public static void printEntropy(MarkovChainv2 mchain){
		Matrix transitionMatrix = mchain.getTransitionMatrix();
		
		double[][] arrayMatrix = new double[transitionMatrix.getRowDimension()][transitionMatrix.getColumnDimension()];
		
		for(int i = 0; i < transitionMatrix.getRowDimension(); i++){
			for(int j = 0; j < transitionMatrix.getColumnDimension(); j++){
				arrayMatrix[i][j] = transitionMatrix.get(i, j);
			}
		}
		
		double entropy = calculateEntropy(arrayMatrix);
		System.out.println("Entropy is :" + entropy);
	}

	public static double calculateEntropy(double[][] transitionMatrix){
		Matrix asymptoticProbabillityMatrix= new Matrix(addOmegaRow(transitionMatrix));
		asymptoticProbabillityMatrix.lu(); //LU decomposision
		double[] omegaValues= new double[asymptoticProbabillityMatrix.getArray().length];

		for(int i = 0; i < asymptoticProbabillityMatrix.getArray().length; i++){
			int rowDimension=asymptoticProbabillityMatrix.getRowDimension();
			double[][] tmp=asymptoticProbabillityMatrix.getArray();
			omegaValues[i]=tmp[i][rowDimension-1];
		}
		double[] statesGoingIn;
		double entropy=0;

		for(int i=0;i<omegaValues.length;i++){
			statesGoingIn=arrayOfStatesGoingIntoIndex(transitionMatrix,i);
			entropy+=omegaValues[i]*findEntropyFromProb(statesGoingIn[i]);
		}
		return entropy;
	}

	//finds "small h in example 4.20"
	private static double findEntropyFromProb(double number){
		double entropySMallh=-log2(number)-log2(1-number);
		return entropySMallh;
	}
	// log2:  Logarithm base 2
	public static double log2(double d) {
		double result = Math.log(d)/Math.log(2.0);
		if(result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY)
			return 0;
		return result;
	}
	
    private static double[][] addOmegaRow(double[][] transitionMatrix){
        double[][] nonLinearOmegaRowMatrice=new double[transitionMatrix.length+1][transitionMatrix.length];
        double[][] outputMatrix=new double[transitionMatrix.length][transitionMatrix.length];
        for(int i = 0 ; i<transitionMatrix.length ; i++){
            for(int j = 0 ; j<transitionMatrix[i].length ; j++){
                nonLinearOmegaRowMatrice[i][j] = transitionMatrix[i][j];
            }
        }
        for(int i=0;i<transitionMatrix[2].length;i++){
            double probOmega=1.0/transitionMatrix[2].length;
            nonLinearOmegaRowMatrice[transitionMatrix.length][i]=probOmega;
        }
        for(int i=0;i<transitionMatrix.length;i++){
            for(int j=0;j<transitionMatrix.length;j++){
                outputMatrix[i][j]=nonLinearOmegaRowMatrice[i+1][j];
            }
        }
        return outputMatrix;
    }
    
    private static double[] arrayOfStatesGoingIntoIndex(double[][] transitionMatrix, int index){
        double[] out= new double[transitionMatrix.length];
        for(int i=0;i<transitionMatrix.length;i++){
        	if(index >= transitionMatrix[i].length)
        		continue;
            out[i]=transitionMatrix[i][index];
        }
        return out;
    }
}
