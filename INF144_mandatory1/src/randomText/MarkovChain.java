package randomText;

import java.util.ArrayList;

import Jama.Matrix;

public class MarkovChain {
	
	char[] alphabet = "abcdefghijklmnopqrstuvwxyzæøå ".toCharArray();
	ArrayList<String> searchSpace = new ArrayList<String>();
	double[][] amountMatrix = new double[alphabet.length][alphabet.length];
	Matrix transitionMatrix = new Matrix(amountMatrix);

	public MarkovChain(String text) {
		ArrayList<String> letters = new ArrayList<String>();
		
		for(int i = 0; i < alphabet.length; i++)
			searchSpace.add(alphabet[i] + "");
		
		for(char letter : alphabet)
			letters.add(letter + "");
		
		for(int i = 0; i < text.length(); i++){
			if(i < text.length() - 2){
				amountMatrix[letters.indexOf(text.charAt(i) + "")][letters.indexOf(text.charAt(i + 1) + "")]++;
			}
			else{
				amountMatrix[letters.indexOf(text.charAt(i) + "")][letters.indexOf(text.charAt(0) + "")]++;
			}
		}
		
		for(int i = 0; i < alphabet.length; i++){
			char currentLetter = alphabet[i];
			int amount = 0;
			
			for(int j = 0; j < text.length(); j++){
				if(text.charAt(j) == currentLetter)
					amount++;
			}
			if(amount != 0){
				for(int j = 0; j < alphabet.length; j++){
					double occurances = amountMatrix[i][j];
					double probability = occurances / (double) amount;
					transitionMatrix.set(i, j, probability);
				}
			}
			else{
				for(int j = 0; j < alphabet.length; j++){
					transitionMatrix.set(i, j, 0);
				}
			}
		}
	}

	public Matrix getTransitionMatrix(){
		return this.transitionMatrix;
	}
	
	public char[] getAlphabet(){
		return this.alphabet;
	}
	
	public ArrayList<String> getSearchSpace(){
		return this.searchSpace;
	}
	
}
