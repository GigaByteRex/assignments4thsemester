package randomText;

import java.util.ArrayList;

import Jama.Matrix;

public class MarkovChainv2 {
	
	char[] alphabet = "abcdefghijklmnopqrstuvwxyzæøå ".toCharArray();
	ArrayList<String> searchSpace = new ArrayList<String>();
	double[][] amountMatrix = new double[alphabet.length][alphabet.length];
	Matrix transitionMatrix;

	public MarkovChainv2(String text, int order) {
		if(order == 0){
			amountMatrix = new double[alphabet.length][alphabet.length];
		}
		else{
			Double size = Math.pow(alphabet.length, order);
			amountMatrix = new double[size.intValue()][alphabet.length];
		}
		transitionMatrix = new Matrix(amountMatrix);
		
		if(order == 0 || order == 1){
			for(int i = 0; i < alphabet.length; i++)
				searchSpace.add(alphabet[i] + "");
			
			for(int i = 0; i < text.length(); i++){
				if(i < text.length() - 2){
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "")][searchSpace.indexOf(text.charAt(i + 1) + "")]++;
				}
				else{
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "")][searchSpace.indexOf(text.charAt(0) + "")]++;
				}
			}
			
			for(int i = 0; i < alphabet.length; i++){
				char currentLetter = alphabet[i];
				int amount = 0;
				
				for(int j = 0; j < text.length(); j++){
					if(text.charAt(j) == currentLetter)
						amount++;
				}
				if(amount != 0){
					for(int j = 0; j < alphabet.length; j++){
						double occurances = amountMatrix[i][j];
						double probability = occurances / (double) amount;
						transitionMatrix.set(i, j, probability);
					}
				}
				else{
					for(int j = 0; j < alphabet.length; j++){
						transitionMatrix.set(i, j, 0);
					}
				}
			}
		}
		
		if(order == 2){
			for(char letter1 : alphabet)
				for(char letter2 : alphabet)
					searchSpace.add(letter1 + "" + letter2 + "");
			
			ArrayList<String> singleLetters = new ArrayList<String>();
			
			for(char letter : alphabet)
				singleLetters.add(letter + "");
			
			for(int i = 0; i < text.length(); i++){
				
				if(i < text.length() - 2){
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "" + text.charAt(i + 1) + "")][singleLetters.indexOf(text.charAt(i + 2) + "")]++;
				}
				else if(i == text.length() - 2){
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "" + text.charAt(i + 1) + "")][singleLetters.indexOf(text.charAt(0) + "")]++;
				}
				else{
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "" + text.charAt(0) + "")][singleLetters.indexOf(text.charAt(1) + "")]++;
				}
			}
			
			for(int i = 0; i < amountMatrix.length; i++){
				double amount = 0;
				
				for(int j = 0; j < amountMatrix[i].length; j++){
					amount += amountMatrix[i][j];
				}
				
				if(amount != 0){
					for(int j = 0; j < amountMatrix[i].length; j++){
						double probability = (int)amountMatrix[i][j] / amount;
						transitionMatrix.set(i, j, probability);
					}
				}
				else{
					for(int j = 0; j < amountMatrix[i].length; j++){
						transitionMatrix.set(i, j, 0);
					}
				}
			}
		}
		
		if(order == 3){
			for(char letter1 : alphabet)
				for(char letter2 : alphabet)
					for(char letter3: alphabet)
						searchSpace.add(letter1 + "" + letter2 + "" + letter3 + "");
			
			ArrayList<String> singleLetters = new ArrayList<String>();
			
			for(char letter : alphabet)
				singleLetters.add(letter + "");
			
			for(int i = 0; i < text.length(); i++){
				
				if(i < text.length() - 3){
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "" + text.charAt(i + 1) + "" + text.charAt(i + 2))][singleLetters.indexOf(text.charAt(i + 3) + "")]++;
				}
				else if(i == text.length() - 3){
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "" + text.charAt(i + 1) + "" + text.charAt(i + 2) + "")][singleLetters.indexOf(text.charAt(0) + "")]++;
				}
				else if( i == text.length() - 2){
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "" + text.charAt(i + 1) + "" + text.charAt(0) + "")][singleLetters.indexOf(text.charAt(1) + "")]++;
				}
				else{
					amountMatrix[searchSpace.indexOf(text.charAt(i) + "" + text.charAt(0) + "" + text.charAt(1) + "")][singleLetters.indexOf(text.charAt(2) + "")]++;
				}
			}
			
			for(int i = 0; i < amountMatrix.length; i++){
				double amount = 0;
				
				for(int j = 0; j < amountMatrix[i].length; j++){
					amount += amountMatrix[i][j];
				}
				
				if(amount != 0){
					for(int j = 0; j < amountMatrix[i].length; j++){
						double probability = (int)amountMatrix[i][j] / amount;
						transitionMatrix.set(i, j, probability);
					}
				}
				else{
					for(int j = 0; j < amountMatrix[i].length; j++){
						transitionMatrix.set(i, j, 0);
					}
				}
			}
		}
	}

	public Matrix getTransitionMatrix(){
		return this.transitionMatrix;
	}
	
	public char[] getAlphabet(){
		return this.alphabet;
	}
	
	public ArrayList<String> getSearchSpace(){
		return this.searchSpace;
	}
	
}
