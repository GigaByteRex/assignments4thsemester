package assignment2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.*;
import assignment2.BarabasiAlbertGen;
import org.graphstream.algorithm.Toolkit;

public class BarabasiAlbert {
	
	public static void main(String[] args) throws Exception{

		Graph graph = new SingleGraph("Barabasi-Albert");
		
		BarabasiAlbertGen BAGen = new BarabasiAlbertGen(4, true);
		
		BAGen.addSink(graph);
		BAGen.begin();
		
		for(int i = 0; i < 10000; i++){
			BAGen.nextEvents();
			if(graph.getNodeCount() == 100)
				System.out.println("Degree distribution 100 nodes: " + Arrays.toString(Toolkit.degreeDistribution(graph)) + "\n");
			
			if(graph.getNodeCount() == 1000)
				System.out.println("Degree distribution 1000 nodes: " + Arrays.toString(Toolkit.degreeDistribution(graph)) + "\n");
			
			if(graph.getNodeCount() == 10000)
				System.out.println("Degree distribution 10000 nodes: " + Arrays.toString(Toolkit.degreeDistribution(graph)) + "\n");
		}
		
		System.out.println("Average clustering coefficient: " + Toolkit.averageClusteringCoefficient(graph));
		
		BAGen.end();
		
		Graph newGraph = new SingleGraph("XalviBrunetSokolov");
		
		BarabasiAlbertGen BAGenXalviBrunetSokolov = new BarabasiAlbertGen(4, true);
		
		BAGenXalviBrunetSokolov.addSink(newGraph);
		
		BAGenXalviBrunetSokolov.begin();
		
		while(newGraph.getNodeCount() < 104)
			BAGenXalviBrunetSokolov.nextEvents();
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Write a for assortative, and d for disassortative:");
		String input = keyboard.nextLine().toLowerCase();
		
		for(int i = 0; i < 10000; i++)
			XalviBrunetSokolov(newGraph, input);
	}
	
	public static Graph XalviBrunetSokolov(Graph graph, String input){
		Edge e1 = graph.getEdge((int)Math.floor(Math.random() * graph.getEdgeCount()));
		Edge e2 = graph.getEdge((int)Math.floor(Math.random() * graph.getEdgeCount()));
		
		while(e1.equals(e2)){
			e1 = graph.getEdge((int)Math.floor(Math.random() * graph.getEdgeCount()));
			e2 = graph.getEdge((int)Math.floor(Math.random() * graph.getEdgeCount()));
		}
		
		Node n1 = e1.getNode0();
		Node n2 = e1.getNode1();
		Node n3 = e2.getNode0();
		Node n4 = e2.getNode1();
		
		ArrayList<Node> nodes = new ArrayList<Node>();
		
		nodes.add(n1); nodes.add(n2); nodes.add(n3); nodes.add(n4);
		
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < nodes.size() - 1; j++){
				if(nodes.get(j).getDegree() < nodes.get(j + 1).getDegree()){
					Node tmp = nodes.get(j);
					nodes.set(j, nodes.get(j + 1));
					nodes.set(j + 1, tmp);
				}
			}
		}
		
		if(input.equals("a")){
			graph.removeEdge(e1);
			graph.removeEdge(e2);
			graph.addEdge(e1.getId(), nodes.get(0), nodes.get(1));
			graph.addEdge(e2.getId(), nodes.get(2), nodes.get(3));
		}
		
		else if(input.equals("d")){
			graph.removeEdge(e1);
			graph.removeEdge(e2);
			graph.addEdge(e1.getId(), nodes.get(0), nodes.get(3));
			graph.addEdge(e2.getId(), nodes.get(1), nodes.get(2));
		}
		
		return graph;
	}
}
