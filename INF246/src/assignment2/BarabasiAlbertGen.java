package assignment2;

import java.util.ArrayList;
import java.util.HashSet;
import org.graphstream.algorithm.generator.*;

public class BarabasiAlbertGen extends BarabasiAlbertGenerator{
	
	public BarabasiAlbertGen(int degrees, boolean exactly){
		super(degrees, exactly);
	}
	
	public void begin(){
		this.addNode("0");
		this.addNode("1");
		this.addNode("2");
		this.addNode("3");
		
		for(int i = 0; i < 4; i++){
			for(int j = i + 1; j < 4; j++){
				this.addEdge(String.valueOf(i) + "_" + String.valueOf(j), String.valueOf(i), String.valueOf(j));
			}
		}
		
		this.degrees = new ArrayList<Integer>();
		this.degrees.add(3);
		this.degrees.add(3);
		this.degrees.add(3);
		this.degrees.add(3);
		
		this.sumDeg = 12;
		
		this.connected = new HashSet<Integer>();
	}
}
