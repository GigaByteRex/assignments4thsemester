package networks;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.*;

public class MakeArtNetwork {

	public static void main(String[] args) throws Exception{
		
		Graph graph = new SingleGraph("BarabásiAlbertNetwork");
		
		graph.display();
		
		graph.addNode("0");
		graph.addNode("1");
		graph.addNode("2");
		
		int identifier = 2;
		
		graph.addEdge("0", "0", "2");
		graph.addEdge("1", "1", "2");
		
		for(int i = 3; i < 1000; i++){
			graph.addNode(String.valueOf(i));
			
			int initialConnection = (int)Math.floor(Math.random() * graph.getNodeCount());
			graph.addEdge(String.valueOf(identifier), String.valueOf(i), String.valueOf(initialConnection));
			
			int edgeSum = 0;
			for(int j = i - 1; j >= 0; j--){
				edgeSum += graph.getNode(String.valueOf(j)).getDegree();
				identifier++;
			}
			
			for(int j = i - 1; j >= 0; j--){
				double prob = (double)graph.getNode(String.valueOf(j)).getDegree() / (double)edgeSum;
				if(Math.random() <= prob && j != initialConnection){
					graph.addEdge(String.valueOf(identifier), String.valueOf(i), String.valueOf(j));
					identifier++;
					Thread.sleep(50);
				}
			}
		}
	}
}
