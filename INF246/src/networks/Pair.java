package networks;

public class Pair<T, U> {         
    public final T t;
    public final U u;

    public Pair(T t, U u) {         
        this.t = t;
        this.u = u;
    }
    
    public T getPart1(){
    	return this.t;
    }
    
    public U getPart2(){
    	return this.u;
    }
}