package networks;

import java.util.Scanner;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.*;

public class RandomNetworks {

	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		Graph graph1 = createGraph(500, 0.8/499);
		Graph graph2 = createGraph(500, 1.0/499);
		Graph graph3 = createGraph(500, 8.0/499);
		
		boolean active = true;
		
		while(active){
			System.out.println("write 1, 2 or 3 to see graphs with respectively average degree of 0.8, 1 and 8. q to quit");
			String choice = input.nextLine();
			
			if(choice.equals("1"))
				graph1.display();
			else if(choice.equals("2"))
				graph2.display();
			else if(choice.equals("3"))
				graph3.display();
			else if(choice.equals("q"))
				System.exit(0);
			else{
				System.out.println("Unknown command, try again.");
			}
		}
		
		input.close();
	}
	
	public static Graph createGraph(int vertexAmount, double probability){
		Graph graph = new SingleGraph("graph");
		
		for(int i = 0; i < vertexAmount; i++){
			graph.addNode(String.valueOf(i));
		}

		int identifier = 0;
		
		for(Node n : graph){
			for(Node n2 : graph){
				double num = Math.random();
				if(n.equals(n2) == false && n.hasEdgeBetween(n2) == false && num <= probability){
					graph.addEdge(String.valueOf(identifier), n, n2);
					identifier++;
				}
			}
		}
		
		return graph;
	}
	
}
