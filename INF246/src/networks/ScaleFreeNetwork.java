package networks;

import java.util.ArrayList;
import java.util.Collections;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.*;

public class ScaleFreeNetwork {

	public static void main(String[] args){
		int[] N = {1000, 10000, 100000};
		double[] Y = {2.2, 3};
		
		for(double y : Y){
			for(int n : N){
				System.out.println("Graph size: " + n);
				System.out.println("Degree exponent: " + y);
				
				Graph graph = createGraph(n);
				ArrayList<Integer> stubList = generateStubList(n, y);
				
				graph = connectGraph(graph, stubList);
				
				System.out.println("Amount of edges: " + graph.getEdgeCount());
				
				int selfLoops = getAmountOfSelfLoops(graph);
				System.out.println("Amount of self loops. " + selfLoops);
				System.out.println("Percentage of self loops: " + ((double)selfLoops / (double)graph.getEdgeCount() * 100.0) + "%");
				
				int multiLinks = getAmountOfMultiLinks(graph);
				System.out.println("Amount of multilinks: " + multiLinks);
				System.out.println("Percentage of multilinks: " + ((double)multiLinks / (double)graph.getEdgeCount() * 100.0) + "%");
				
				
				
				System.out.println();
				
			}
		}
	}
	
	public static Graph createGraph(int n){
		Graph graph = new MultiGraph("graph");
		
		for(int i = 0; i < n; i++){
			graph.addNode(String.valueOf(i));
		}
		
		return graph;
	}

	public static ArrayList<Integer> generateStubList(int n, double y){
		
		double C = 0;
		
		for(int i = 1; i <= 100000; i++)
			C += Math.pow(i, -y);
		
		ArrayList<Double> pk = new ArrayList<Double>();
		
		for(int i = 1; i < n + 1; i++){
			if(i > 1)
				pk.add(( Math.pow(i, -y) / C ) + pk.get(i - 2));
			else
				pk.add( Math.pow(i, -y) / C );
		}
		
		boolean even = false;
		ArrayList<Integer> stubList = new ArrayList<Integer>();
		ArrayList<Double> randomNumbers = new ArrayList<Double>();
		
		while(!even){
			stubList.clear();
			randomNumbers.clear();
			
			for(int i = 0; i < n; i++)
				randomNumbers.add(Math.random());
			
			for(double num : randomNumbers){
				int i = 0;
				int k = 0;
				
				while(num > pk.get(i) && i < n - 1){
					k = i;
					i += 1;
				}
				stubList.add(k + 1);
			}
			
			int sum = 0;
			
			for(int num : stubList)
				sum += num;
			
			even = sum % 2 == 0;
		}
		
		return stubList;
	}
	
	public static Graph connectGraph(Graph graph, ArrayList<Integer> stubList){
		
		int numOfStubs = 0;
		
		for(int num : stubList)
			numOfStubs += num;
		
		ArrayList<Integer> stubToNodeList = new ArrayList<Integer>();
		int counter = 0;
		
		for(int i = 0; i < stubList.size(); i++){
			int degree = stubList.get(i);
			
			for(int j = counter; j < counter + degree; j++)
				stubToNodeList.add(i);
			
			counter = counter + degree;
		}
		
		ArrayList<Integer> stubs = new ArrayList<Integer>();
		
		for(int i = 0; i < numOfStubs; i++)
			stubs.add(i);
		
		Collections.shuffle(stubs);
		
		for(int i = 0; i < numOfStubs; i += 2){
			int stub1 = stubs.get(i);
			int stub2 = stubs.get(i + 1);
			graph.addEdge(String.valueOf(graph.getEdgeCount()), String.valueOf(stubToNodeList.get(stub1)), String.valueOf(stubToNodeList.get(stub2)));
		}
		
		return graph;
	}
	
	public static int getAmountOfSelfLoops(Graph graph){
		int amount = 0;
		
		for(Edge edge : graph.getEachEdge())
			if(edge.getNode0() == edge.getNode1())
				amount++;
		
		return amount;
	}
	
	public static int getAmountOfMultiLinks(Graph graph){
		int amount = 0;
		
		ArrayList<Pair<Node, Node>> unique = new ArrayList<Pair<Node, Node>>();
		
		for(Edge edge : graph.getEachEdge()){
			if(unique.size() > 0){
				for(int i = 0; i < unique.size(); i++){
					Pair<Node, Node> current = unique.get(i);
					if(edge.getNode0() == current.getPart1() && edge.getNode1() == current.getPart2()){
						amount++;
						break;
					}
					else if(i == unique.size() - 1){
						unique.add(new Pair<Node, Node>(edge.getNode0(), edge.getNode1()));
						break;
					}
				}
			}
			else{
				unique.add(new Pair<Node, Node>(edge.getNode0(), edge.getNode1()));
			}
		}
		
		return amount;
	}
}