package networks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.*;

public class ScaleFreeNetworkAttempt1 {
	
	public static void main(String[] args){
		int[] N = {1000, 600000, 50000};
		double[] Y = {2.2, 3.0};
		
		for(double y : Y){
			for(int n : N){
				ArrayList<Integer> degreeSequence = genereteDegreeSequence(n, y);
				
				HashMap<Integer, Integer> edgeList = confModel(degreeSequence);
				
				//double percentageMultiEdges = getPercentageMultiEdges(edgeList);
			}
		}
	}
	
	public static ArrayList<Integer> genereteDegreeSequence(int n, double y){
		double C = 0.0;
		for(int i = 0; i < 100000; i++)
			C += Math.pow(i, -y);
		
		ArrayList<Double> pk = new ArrayList<Double>();
		
		for(int i = 0; i < n + 1; i++){
			if(i > 1)
				pk.add((Math.pow(i, -y) / C) + pk.get(i - 2));
			else
				pk.add(Math.pow(i, -y) / C);
		}
		
		boolean even = false;
		ArrayList<Integer> stubList = new ArrayList<Integer>();
		ArrayList<Double> randomNumbers = new ArrayList<Double>();
		
		while(!even){
			stubList.clear();
			randomNumbers.clear();
			
			for(int i = 0; i < n; i++)
				randomNumbers.add(Math.random());
			
			for(double num : randomNumbers){
				int i = 0;
				int k = 0;
				while(num > pk.get(i) && i < n - 1){
					k = i;
					i += 1;
				}
				stubList.add(k + 1);
			}
			
			int sum = 0;
			for(int num : stubList)
				sum += num;
			
			even = sum % 2 == 0;
		}
		
		return stubList;
	}
	
	public static HashMap<Integer, Integer> confModel(ArrayList<Integer> degreeSequence){
		int numOfStubs = 0;
		
		for(int num : degreeSequence)
			numOfStubs += num;
		
		ArrayList<Integer> stubToNodeID = new ArrayList<Integer>();
		int current = 0;
		
		for(int i = 0; i < degreeSequence.size(); i++){
			int degree = degreeSequence.get(i);
			for(int j = current; j < current + degree; j++)
				stubToNodeID.set(j, i);
			current = current + degree;
		}
		
		ArrayList<Integer> stubs = new ArrayList<Integer>();
		
		for(int i = 0; i < numOfStubs; i++){
			stubs.add(i);
		}
		
		Collections.shuffle(stubs);
		
		HashMap<Integer, Integer> edges = new HashMap<Integer, Integer>();
		
		for(int i = 0; i < numOfStubs; i += 2){
			int stubOne = stubs.get(i);
			int stubTwo = stubs.get(i + 1);
			edges.put(stubToNodeID.get(stubOne), stubToNodeID.get(stubTwo));
		}
		
		return edges;
	}
	/*
	public static double getPercentageMultiEdges(HashMap<Integer, Integer> edgeList){
		int numOfMultiedges = getNumOfMultiedges(edgeList);
	}
	
	public static int getNumOfMultiedges(HashMap<Integer, Integer> edgeList){
		int numOfMultiedges = 0;
		ArrayList<Integer> dic = new ArrayList<Intereg>();
		
		for(int i = 0; i < edgeList.size(); i++){
			
		}
	}
	*/
}